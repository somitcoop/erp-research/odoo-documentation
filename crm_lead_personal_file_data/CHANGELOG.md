# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [12.0.0.1.0] - 2024-05-10
### Added
- [#2](https://gitlab.com/somitcoop/erp-research/odoo-documentation/-/merge_requests/2) Add crm_lead_personal_file_data module