# Copyright 2023-SomItCoop SCCL(<https://gitlab.com/somitcoop>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "SomItCoop Odoo CRM Lead DMS API",
    "version": "12.0.0.1.0",
    "depends": [
        "crm_lead_token",
        "crm_lead_dms_category",
        "api_common_base",
    ],
    "author": """
        Som It Cooperatiu SCCL,
        Som Connexió SCCL,
        Odoo Community Association (OCA)
    """,
    "category": "Customer Relationship Management",
    "website": "https://gitlab.com/somitcoop/erp-research/odoo-helpdesk",
    "license": "AGPL-3",
    "summary": """
API to check if a CRM Lead has all the required documents
    """,
    "data": [],
    "application": False,
    "installable": True,
}
